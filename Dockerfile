FROM peuybul/postgresql:12

RUN apt update ; apt upgrade -y
RUN ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime && \
    echo "Asia/Jakarta" > /etc/timezone

CMD ["postgres"]